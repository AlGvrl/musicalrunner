An old prototype of a runner (think Canabalt or Flood Runner) game in which player is supposed to land on platforms in time with beat/rhythm.

I think nowadays Crypt of the NecroDancer implements similar mechanics for movement, but on top of a rogue-like rather than on top of a runner.
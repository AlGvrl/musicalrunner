package {
	import org.flixel.*;

	/**
	 * @author 1
	 */
	public class LevelPart extends FlxGroup {
		
		private static var _partWidth:int=-1;
		private static var _partHeight:int=-1;
		private static var _totalParts:int=0;
		
		private var _blocks:Vector.<LevelBlock>;
		private var _row:int;     //0-2
		private var _column:int;  //0-3
		private var _numberBlock:FlxText;
		
		public function LevelPart(x:int,y:int,noInit:Boolean=false, MaxSize : uint = 0) {
			super(MaxSize);
			_row=x;
			_column=y;
			if (!noInit)
				init();
		}
		
		private function init():void
		{
			if (_partWidth==-1||_partHeight==-1)
			{
				_partWidth=FlxG.width;
				_partHeight=FlxG.height;
			}
			
			_blocks=new Vector.<LevelBlock>();
			
			switch ((_row*4+_column)%3)
				{
					case 0:
				  	LoadLayout0();
				 	break;
					
					case 1:
					LoadLayout1();
					break;
					
					case 2:
					LoadLayout2();
					break;
				}
		}
		
		
		
		
		// Heikki's attempt on the new layouts
		
			private function LoadLayout0():void
		{
			if (_partWidth==-1||_partHeight==-1)
			{
				_partWidth=FlxG.width;
				_partHeight=FlxG.height;
			}
			
			_blocks=new Vector.<LevelBlock>();
			
			addBlock(0, 460, 40, 20, Notes.C);
			addBlock(100, 440, 40, 20, Notes.D);
			addBlock(200, 420, 40, 20, Notes.E);
			addBlock(300, 400, 40, 20, Notes.F);
			addBlock(400, 380, 40, 20, Notes.G);
			addBlock(500, 360, 40, 20, Notes.A);
			addBlock(600, 340, 40, 20, Notes.B);
			
			_numberBlock=new FlxText(_column*_partWidth+270, _row*_partHeight+40, 40);
			_numberBlock.text=LevelPart._totalParts.toString();
			_totalParts++;
			add(_numberBlock);
		}
		
			private function LoadLayout1():void
		{
			if (_partWidth==-1||_partHeight==-1)
			{
				_partWidth=FlxG.width;
				_partHeight=FlxG.height;
			}
			
			_blocks=new Vector.<LevelBlock>();
			
			addBlock(0, 460, 640, 20, Notes.NONE);
			addBlock(150, 300, 60, 20, Notes.E);
			addBlock(300, 240, 60, 20, Notes.C);
			addBlock(450, 330, 80, 20, Notes.G);
			addBlock(600, 240, 60, 20, Notes.F);
			
			_numberBlock=new FlxText(_column*_partWidth+600, _row*_partHeight+40, 40);
			_numberBlock.text=LevelPart._totalParts.toString();
			_totalParts++;
			add(_numberBlock);
		}
		
				private function LoadLayout2():void
		{
			if (_partWidth==-1||_partHeight==-1)
			{
				_partWidth=FlxG.width;
				_partHeight=FlxG.height;
			}
			
			_blocks=new Vector.<LevelBlock>();
			
			addBlock(20, 460, 300, 20, Notes.NONE);
			addBlock(320, 400, 300, 20, Notes.NONE);
			
			addBlock(220, 200, 90, 20, Notes.E);
			
			_numberBlock=new FlxText(_column*_partWidth+600, _row*_partHeight+40, 40);
			_numberBlock.text=LevelPart._totalParts.toString();
			_totalParts++;
			add(_numberBlock);
		}
		
		private function LoadLayout3():void
		{
			if (_partWidth==-1||_partHeight==-1)
			{
				_partWidth=FlxG.width;
				_partHeight=FlxG.height;
			}
			
			_blocks=new Vector.<LevelBlock>();
			
			addBlock(0, 200, 90, 20, Notes.C);
			addBlock(110, 120, 90, 20, Notes.D);
			addBlock(220, 40, 90, 20, Notes.E);
			addBlock(330, 200, 90, 20, Notes.F);
			addBlock(440, 120, 90, 20, Notes.G);
			addBlock(550, 40, 90, 20, Notes.A);
			addBlock(0, 200, 90, 20, Notes.B);
			
			_numberBlock=new FlxText(_column*_partWidth+600, _row*_partHeight+40, 40);
			_numberBlock.text=LevelPart._totalParts.toString();
			_totalParts++;
			add(_numberBlock);
		}
		
		// Heikki's attempt ends here..
		
		public function addBlock (x:int ,y:int, width:int, height:int, note:int):void
		{
			_blocks.push(new LevelBlock(_column*_partWidth+x,_row*_partHeight+y,note).blMakeGraphic(width, height));
			add(_blocks[_blocks.length-1]);
		}
		
		public function addFromBlock (block:LevelBlock):void//block's coords should be screen-relative--Uhm nope
		{
			//block.x+=_column*_partWidth;
			//block.y+=_row*_partHeight;
			if (block==null)
				trace ("Null inside adding");
			_blocks.push(block);
			add(_blocks[_blocks.length-1]);	
		}

		public function clone():LevelPart
		{
			var result:LevelPart=new LevelPart(0, 0, true);
			result._row=_row;
			result._column=_column;
			result._blocks=new Vector.<LevelBlock>();
			result._numberBlock=new FlxText(_column*_partWidth+270, _row*_partHeight+40, 40);
			for each (var i:LevelBlock in _blocks)
			{
				if (i==null)
					trace ("Null inside part cloning");
				result.addFromBlock(i.clone());
			}
			return result;	
		}
		
		public function moveTo (newRow:int=0,newColumn:int=0):void
		{//testing new master
			for each (var i:LevelBlock in _blocks)
			{
				//trace ("was",i.x,i.y);
				i.x+=(newColumn-_column)*_partWidth;
				i.y+=(newRow-_row)*_partHeight;
				//trace(i.x,i.y);
			}
			_row=newRow;
			_column=newColumn;
		}
		
		public static function get partWidth():int 
		{
			return _partWidth;
		}
		
		public static function get partHeight():int 
		{
			return _partHeight;
		}
	}
}

package {
	import org.flixel.*;

	/**
	 * @author 1
	 */
	 
	public class MenuState extends FlxState {
		public function MenuState() {
		}
		
		override public function create():void
		{
			super.create();
			var playButton:FlxButton=new FlxButton(FlxG.width/2-80,FlxG.height/3-20,"Play",onPlayClicked);
			playButton.scale.x=2;
			playButton.scale.y=2;
			var createButton:FlxButton=new FlxButton(FlxG.width/2-80,2*FlxG.height/3-20,"Create Level",onCreateClicked);
			createButton.scale.x=2;
			createButton.scale.y=2;
			add(playButton);
			add(createButton);
			var cam:FlxCamera=new FlxCamera(0, 0, FlxG.width, FlxG.height);
			FlxG.addCamera(cam);
			FlxG.mouse.show();
		}
		
		private function onPlayClicked():void
		{
			FlxG.switchState(new PlayState());
		}
		
		private function onCreateClicked():void
		{
			FlxG.switchState(new EditorState());
		}
	}
}

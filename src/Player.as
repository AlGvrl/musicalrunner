package {
	import org.flixel.*;

	/**
	 * @author 1
	 */
	public class Player extends FlxSprite {
		
		public var isWhite:Boolean=true;
		private var _jumpTime:Number;
		private var _maxJumpTime:Number;
		private var _dropped:Boolean;
		private var _defaultMaxVelocity:FlxPoint;
		private var _boost:Number;//0.05--1.0 (from 10% of default speed and up to 200%)
		private var _isReadyForBoosting:Boolean;
		private var _boostDecreaseRate:Number;//seconds
		private var _boostDecreaseAmount:Number;
		private var _boostDecreaseTimer:Number;//seconds
		private var _boostIncreaseOnLanding:Number;
		
		public function Player(X : Number = 0, Y : Number = 0, SimpleGraphic : Class = null) {
			super(X, Y, SimpleGraphic);
			init();
		}
		
		private function init ():void
		{
			//just initializing variables
			_dropped=false;
			_defaultMaxVelocity=new FlxPoint(400,1200);
			maxVelocity=new FlxPoint().copyFrom(_defaultMaxVelocity);
			acceleration.y=1200;
			drag.x=maxVelocity.x*2;
			_maxJumpTime=0.45;
			_jumpTime=0;
			_boost=0.5;
			_isReadyForBoosting=true;
			_boostDecreaseRate=0.2;
			_boostDecreaseAmount=0.01;
			_boostIncreaseOnLanding=0.1;
			_boostDecreaseTimer=_boostDecreaseRate;
		}
		
		//copypasted this one from FlxSprite since i couldn't override method in such a way it returns another type
		//some dark magic here
		public function plMakeGraphic(Width:uint,Height:uint,Color:uint=0xffffffff,Unique:Boolean=false,Key:String=null):Player
		{
			_bakedRotation = 0;
			_pixels = FlxG.createBitmap(Width,Height,Color,Unique,Key);
			width = frameWidth = _pixels.width;
			height = frameHeight = _pixels.height;
			resetHelpers();
			return this;
		}
		
		public function onBeatStarted():void
		{
			_isReadyForBoosting=true;
			color=0xffff0000;
			isWhite=false;
		}
		
		public function onBeatStopped():void
		{
			_isReadyForBoosting=false;
			color=0xffffffff;
			isWhite=true;
		}
		
		//now, how update works
		//state actually inherits from group, so update in them is same
		//and in group update just goes through all the group members and calls update method of every of them
		//that's why we needed to call "super.update" in state
		override public function update ():void
		{
			super.update();
			//trace(_boost,_isReadyForBoosting);
			//recalculating boost. Only affect speed on X now
			if (_boostDecreaseTimer<0)
			{
				_boost-=_boostDecreaseAmount;
				_boostDecreaseTimer=_boostDecreaseRate;	
				//_isReadyForBoosting=true;//That's a bad way, but that should do it for now
			}
			else
				_boostDecreaseTimer-=FlxG.elapsed;
			_boost=_boost>1.0?1.0:_boost;
			_boost=_boost<0.1?0.1:_boost;
			maxVelocity.x=_defaultMaxVelocity.x*2*_boost;
			//trace (maxVelocity.x,_defaultMaxVelocity.x);
			
			acceleration.x = 0;
			//everything below is just processing outputs
			if (_dropped&&isTouching(FlxObject.FLOOR))
			{
				_dropped=false;
			}

			//jumping
			if (isTouching(FlxObject.FLOOR))
			{
				_jumpTime=0;
			}
			
			if (_jumpTime>=0&& ((FlxG.keys.UP)||(FlxG.keys.SPACE)||(FlxG.keys.W)))
			{
				_jumpTime+=FlxG.elapsed;
				if (_jumpTime>_maxJumpTime) 
				{
					_jumpTime=-1;
				}
			}
			else
			{
				_jumpTime=-1;
			}
			
			if (_jumpTime>0)
			{
				if (_jumpTime<0.065)//was 0.065
				{
					velocity.y=-2*maxVelocity.y/5; //tweak this to change min height
				}
				else
				{
					acceleration.y=100;//tweak this or the condition to change max height
				}
			}
			else
			{
				acceleration.y=1200;	
			}
			//movement
			if (FlxG.keys.A||FlxG.keys.LEFT)
			{
				acceleration.x -= maxVelocity.x * 4;
			}
			if (FlxG.keys.D||FlxG.keys.RIGHT)
			{
				acceleration.x += maxVelocity.x * 4;
			}
			/* old jumping
			if ((FlxG.keys.justPressed("W")||FlxG.keys.justPressed("UP") ||FlxG.keys.justPressed("SPACE")) 
				&& isTouching(FlxObject.FLOOR))
			{
				velocity.y -= maxVelocity.y/2;
			}*/
			//dropping
			if ((FlxG.keys.justPressed("DOWN")||FlxG.keys.justPressed("S")) 
				&&!_dropped)
			{
				velocity.y+=maxVelocity.y;
				_dropped=true;
			}
			if (FlxG.keys.R)
			{
				//reset the values to starting ones
				velocity.x=0;
				velocity.y=0;
				x=65;
				y=50;
			}
		}
		
		public function boostByDefaultValue ():void
		{ 
			trace ("boosted from", _boost);
			if (_isReadyForBoosting)
				_boost+=_boostIncreaseOnLanding;
			trace ("boosted to", _boost);
			//_isReadyForBoosting=false;
		}
		
	}
}

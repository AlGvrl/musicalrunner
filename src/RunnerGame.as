package {
	import org.flixel.*;
	/**
	 * @author 1
	 */
	 
	[SWF (width="640", height="480",visible="true")]
	
	//main class and entrance point of all the application
	public class RunnerGame extends FlxGame {
		public function RunnerGame ():void
		{
			//create a game 320px wide, 240px high, with default state being PlayState
			//Also zoomed 2x, 60 times per second logic update, 30 frames per second and something i don't remember
			Notes.init();
			super(640,480, MenuState,1,60,30,false);
		}
	}
}

package {
	import org.flixel.FlxSound;
	import org.flixel.FlxG;
	import org.flixel.FlxSprite;

	/**
	 * @author 1
	 */
	public class LevelBlock extends FlxSprite {
		
		private var _pitch:int;
		private var _prevSound:FlxSound;
		
		public function LevelBlock(X : Number = 0, Y : Number = 0, pitch:int=Notes.NONE, SimpleGraphic : Class = null) {
			super(X, Y, SimpleGraphic);
			_pitch=pitch;
			init();
		}
		
		private function init():void
		{
			immovable=true;
		}
		
		public function blMakeGraphic(Width:uint,Height:uint, opacity:uint=0xff, Unique:Boolean=false,Key:String=null):LevelBlock
		{
			_bakedRotation = 0;
			var semicolor:uint=(opacity<<24)|0x00ffffff&Notes.colors[_pitch];
			//trace("in make graphics",semicolor.toString(16));
			_pixels = FlxG.createBitmap(Width,Height,semicolor,Unique,Key);
			width = frameWidth = _pixels.width;
			height = frameHeight = _pixels.height;
			resetHelpers();
			return this;
		}
		
		public function changePitchTo(newPitch:uint):void
		{
			_pitch=newPitch;
			this.blMakeGraphic(width, height);
		}
		
		public function clone():LevelBlock
		{
			var result:LevelBlock=new LevelBlock(x,y,_pitch).blMakeGraphic(width, height);
			return result;
		}
		
		public function onCollision():void
		{
			if (_pitch==Notes.NONE)
    			return;
			
			if (_prevSound==null||!_prevSound.active)
			{
				_prevSound=FlxG.play(Notes.sounds[_pitch]);
			}
		}
		
		public function get notANote():Boolean
		{
			return (_pitch==Notes.NONE);
		}
		
		public function get pitch():uint
		{
			return _pitch;
		}
	}
}

package {
	import org.flixel.*;

	/**
	 * @author 1
	 */
	public class Instrument extends FlxBasic {
		
		private var _startWaitingTime:uint;//everything with "time" in name is going to be in milliseconds
		//private var _hasStarted:Boolean; //maybe it is not needed?
		private var _timeInterval:uint;
		private var _tolerance:uint;
		private var _unloadedSound:Class;
		private var _sound:FlxSound;
		private var _wasBeating:Boolean;
		private var _isBeating:Boolean;
		private var _callOnBeatStarted:Vector.<Function>;
		private var _callOnBeatStopped:Vector.<Function>;
		private var _manager:BeatManager;
		
		public function get justStarted():Boolean
		{
			return ((!_wasBeating)&&_isBeating);	
		}
		
		public function get justStopped():Boolean
		{
			return (_wasBeating&&(!_isBeating));
		}
		
		public function get isBeating():Boolean
		{
			return _isBeating;
		}
		
		public function get unloadedSound():Class
		{
			return _unloadedSound;
		}
		
		public function Instrument(wait:uint,interval:uint,tolerance:uint,sound:Class, manager:BeatManager) {
			super();
			_startWaitingTime=wait;
			_timeInterval=interval;
			_tolerance=tolerance;
			_unloadedSound=sound;
			_sound=new FlxSound().loadEmbedded(sound);
			_manager=manager;
			init();
		}
		
		private function init():void
		{
			//_hasStarted=false;
			_wasBeating=false;
			_isBeating=false;
			_callOnBeatStarted=new Vector.<Function>();
			_callOnBeatStopped=new Vector.<Function>();
		}
		
		public function AddStartCallback(func:Function):void
		{
			_callOnBeatStarted.push(func);
		}
		
		public function AddStopCallback(func:Function):void
		{
			_callOnBeatStopped.push(func);
		}
	
		
		public override function update():void
		{
			super.update();
			
			_wasBeating=_isBeating;
			if (((_manager.measureTime-_startWaitingTime)%_timeInterval<=_tolerance)&&
				(_manager.measureTime-_startWaitingTime>=0))
				_isBeating=true;
			else
				_isBeating=false;
			//trace("Update",_isBeating,_wasBeating,_manager.measureTime, (_manager.measureTime-_startWaitingTime)%_timeInterval);
			if (this.justStarted)
			{
				//trace ("trying to play sound",FlxU.getTicks());
				_sound.stop();
				_sound.play();
				for each (var func:Function in _callOnBeatStarted)
					func();
			}
			if (this.justStopped)
			{
				for each (func in _callOnBeatStopped)
					func();
			}
		}
		
	}
}

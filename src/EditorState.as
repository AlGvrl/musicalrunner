package {
	import flash.net.FileReference;
	import flash.sensors.Accelerometer;
	import org.flixel.*;

	/**
	 * @author 1
	 */
	public class EditorState extends FlxState {
		
		private static const tAddBlock:int=1;
		private static const tChangeNote:int=2;
		private static const tDeleteBlock:int=3;
		
		private var _saveButton:FlxButton;
		private var _clearButton:FlxButton;
		private var _addBlockToolButton:FlxButton;
		private var _deleteBlockToolButton:FlxButton;
		private var _changeNoteToolButton:FlxButton;
		private var _toolSelected:int;
		private var _mainCamera:FlxCamera;
		//private var _previewCamera:FlxCamera;//maybe this one isn't even needed. Also, let the tile size be 20x20 in game and 15x15 in editor.
		private var _blocks:Vector.<LevelBlock>;
		private var _previewBlock:LevelBlock;
		
		public function EditorState() {
		}
		
		public override function create ():void
		{
			super.create();
			_blocks=new Vector.<LevelBlock>();
			
			_addBlockToolButton=new FlxButton(520,40,"Add block", switchToolToAdd);//magic constants as coords
			_deleteBlockToolButton=new FlxButton(520,80,"Delete", switchToolToDelete);//change to flxg.width/height
			_changeNoteToolButton=new FlxButton(520,120,"Change pitch", switchToolToChangeNote);//later...
			_clearButton=new FlxButton(520, 360, "Clear", clearEditor);
			_saveButton=new FlxButton(520,400,"Save to XML", saveToXML);
			add(_addBlockToolButton);
			add(_deleteBlockToolButton);
			add(_changeNoteToolButton);
			add(_clearButton);
			add(_saveButton);
			
			var bgRect:FlxSprite=new FlxSprite(0,0).makeGraphic(480, 360,0xffeeeeee);//by default the size of part is 32x24 blocks
			bgRect.solid=false;//so, i'll do this in a bad (but short) way
			bgRect.immovable=true;//in the addBlock function
			add(bgRect);
			
			_mainCamera=new FlxCamera(0, 0, FlxG.width, FlxG.height);
			_mainCamera.bgColor=0xffffffff;
			FlxG.addCamera(_mainCamera);
			
			_previewBlock=new LevelBlock(0,0,Notes.C);
			_previewBlock.blMakeGraphic(15, 15, 0x88);
			add(_previewBlock);
			//trace(_previewBlock.color.toString(16));
			
			_toolSelected=tAddBlock;
		}
		
		public override function update():void
		{
			super.update();
			
			switch (_toolSelected)
			{
				case (tAddBlock):
				{
					movePreviewBlock();
					checkBlockAdding();
					break;	
				}
				case (tDeleteBlock):
				{
					checkBlockDeleting();
				}
				case (tChangeNote):
				{
					checkNoteChanging();	
				}
			}
		}
		
		private function addBlock(column:int, row:int):void//column--"x", row--"y"
		{
			if ((column<0)||(column>31)||(row<0)||(row>23))
				return;
			
			_blocks.push(new LevelBlock(column*15,row*15,_previewBlock.pitch));
			_blocks[_blocks.length-1].blMakeGraphic(15, 15);
			add(_blocks[_blocks.length-1]);
		}
		
		private function checkBlockAdding():void
		{
			var mouseCoordX:int=FlxG.mouse.x-FlxG.mouse.x%15;
			var mouseCoordY:int=FlxG.mouse.y-FlxG.mouse.y%15;
			if (FlxG.mouse.pressed()&&!placeIsTaken(mouseCoordX/15, mouseCoordY/15))
			{
				addBlock(mouseCoordX/15, mouseCoordY/15);
			}
		}
		
		private function checkNoteChanging():void
		{
			var mouseCoordX:int=FlxG.mouse.x-FlxG.mouse.x%15;
			var mouseCoordY:int=FlxG.mouse.y-FlxG.mouse.y%15;
			if (FlxG.mouse.justPressed())
			{
				var block:LevelBlock=getBlockFrom(mouseCoordX/15, mouseCoordY/15);
				if (block!=null)
				{
					block.changePitchTo((block.pitch+1)%8);
					_previewBlock.changePitchTo(block.pitch);//this is not neccessary, but IMO might be useful
				}
			}
		}
		
		private function checkBlockDeleting():void
		{
			var mouseCoordX:int=FlxG.mouse.x-FlxG.mouse.x%15;
			var mouseCoordY:int=FlxG.mouse.y-FlxG.mouse.y%15;
			if (FlxG.mouse.pressed())
			{
				var block:LevelBlock=getBlockFrom(mouseCoordX/15, mouseCoordY/15);
				if (block!=null)
				{
					remove(block);
					_blocks.splice(_blocks.indexOf(block), 1);
					//TODO:call block.delete when it's written
				}
			}
		}
		
		private function clearEditor():void
		{
			for each (var bl:LevelBlock in _blocks)
			{
				remove(bl);
				//TODO:bl.delete
			}
			_blocks.splice(0, _blocks.length);
		}
		
		private function getBlockFrom(column:int, row:int):LevelBlock
		{
			var xc:int=column*15;
			var yc:int=row*15;
			//trace (xc,yc);
			for each (var bl:LevelBlock in _blocks)
				if ((bl.x==xc)&&(bl.y==yc))
					return bl;
			return null;
		}
		
		private function movePreviewBlock():void
		{
			_previewBlock.visible=true;
			
			_previewBlock.x=FlxG.mouse.x-FlxG.mouse.x%15;
			_previewBlock.y=FlxG.mouse.y-FlxG.mouse.y%15;
			
			if (placeIsTaken(_previewBlock.x/15, _previewBlock.y/15))
				_previewBlock.visible=false;
				
			if (_previewBlock.x>=480||_previewBlock.y>=360)
				_previewBlock.visible=false;
		}
		
		private function placeIsTaken(column:int,row:int):Boolean//i brainfarted with these coords, might be bugging 
		{
			return (getBlockFrom(column, row)!=null);
		}
		
		private function saveToXML():void
		{
			var XMLToSave:XML=
				<Part/>;
			var blockXML:XML;
			trace(XMLToSave.toString());
			for each (var bl:LevelBlock in _blocks)
			{
				//pass row and column numbers in xml, not the coords
				blockXML=
					<Block>
						<Row>{bl.y/15}</Row>
						<Column>{bl.x/15}</Column>
						<Note>{bl.pitch}</Note>
					</Block>;
				XMLToSave.appendChild(blockXML);
			}
			trace(XMLToSave.toString());
			var file:FileReference=new FileReference();
			file.save(XMLToSave,"Level.xml");
		}
		
		private function switchToolToAdd():void
		{
			_toolSelected=tAddBlock;
			_previewBlock.visible=true;
		}
		
		private function switchToolToChangeNote():void
		{
			_toolSelected=tChangeNote;
			_previewBlock.visible=false;	
		}
		
		private function switchToolToDelete():void
		{
			_toolSelected=tDeleteBlock;
			_previewBlock.visible=false;	
		}
	}
}

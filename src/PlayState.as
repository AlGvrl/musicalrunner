package {
	import org.flixel.*;

	/**
	 * @author 1
	 */
	public class PlayState extends FlxState {
		//this class inherits from FlxState
		
		//class member variables here
		private var _collisionGroup:FlxGroup;
		private var _player:Player;
		private var _prevColumn:int;
		private var _beat:BeatManager;
		
		private var _allParts:Vector.<Vector.<LevelPart>>;
		
		//constructor. Just the same as parent's (superclass) one
		public function PlayState() 
		{
			super();
		}
		
		//this is called when state is created
		//e.g. when game starts with this as default or when you change state
		override public function create():void
		{
			super.create();//this just needs be done
			trace ("Creating PlayState");//console output in debugging mode
			FlxG.worldBounds=new FlxRect(0,0,FlxG.width*4,FlxG.height*3);//let the world size be 4 screens wide and 3 screen high
			
			//FlxG.playMusic(Resources.bBeat,0.5);
			//var testsound:FlxSound=new FlxSound().loadEmbedded(Resources.bBeat,true);
			//testsound.play();
			
			_collisionGroup=new FlxGroup();
			add(_collisionGroup);//add the group to the state's members, so all of the group member will be displayed 
			
			//instantiate a _player with 10px wide, 12px high rectangle at location 65,200 and colored 0xff220022 as 0xAARRGGBB
			_player=new Player(65,50).plMakeGraphic(15,18,0xffffffff);
			add(_player);
			_prevColumn=3;
			
			_beat=new BeatManager(2000);
			add(_beat);
			var instr:Instrument=_beat.addInstrument(250, 750, 250, Resources.bSnare);
			instr.AddStartCallback(_player.onBeatStarted);
			instr.AddStopCallback(_player.onBeatStopped);
			add(instr);
			//trace(_beat.SelectBySound(Resources.bSnare)==instr);//testing select method
			add (_beat.addInstrument(0, 250, 25, Resources.bHiHat));
			//add (_beat.addInstrument(500, 400, 100, Resources.nA));
			
			_allParts=new Vector.<Vector.<LevelPart>>();
			for (var i:int=0;i<3;i++)
			{
				_allParts.push(new Vector.<LevelPart>());	
				for (var j:int=0;j<4;j++)
				{
					_allParts[i].push(new LevelPart(i, j));
					_collisionGroup.add(_allParts[i][j]);
				}
			}
			
			var cam:FlxCamera = new FlxCamera(0, 0, FlxG.width, FlxG.height);//camera coords and size, i believe
			cam.setBounds(0, 0, FlxG.width*4, FlxG.height*3);//the camera won't go out of the bounds
			//the rest is self-explainatory, i think
			cam.bgColor = 0xff000000;
			cam.follow(_player);
			FlxG.addCamera(cam);	
			
			//var boostCam:FlxCamera=new FlxCamera(X, Y, Width, Height);
		}
		
		//wrote a bit of explanation on updates in PLayer class
		override public function update ():void
		{
			super.update();//needs to be done
			generateNewParts();
			/*if (_beat.isSnareTime())
			{
				//trace ("Badum-tss");
				if (_player.isWhite)
				{
					//trace ("Fail color applied");
					_player.color=0xff5555;
					_player.isWhite=false;
				}
			}
			else
			{
				_player.color=0xffffffff;
				_player.isWhite=true;
			}*/
			FlxG.collide(_player,_collisionGroup,catchCollisions);//collide all the group members with each other
			//you can also collide one group with another
		}
		
		private function catchCollisions(obj1:FlxBasic, obj2:FlxBasic):void
		{
			if ((obj1 is Player)&&(obj2 is LevelBlock)&&_player.justTouched(FlxObject.FLOOR))
			{
				if (!_player.isWhite&&_player.justTouched(FlxObject.FLOOR)) _player.color=0xff00ff00;
				(obj2 as LevelBlock).onCollision();
				if (!(obj2 as LevelBlock).notANote)
					_player.boostByDefaultValue();
			}
			if ((obj1 is LevelBlock)&&(obj2 is Player)&&_player.justTouched(FlxObject.FLOOR))
			{
				(obj1 as LevelBlock).onCollision();
				if (!_player.isWhite&&_player.justTouched(FlxObject.FLOOR)) _player.color=0xff00ff00;
				if (!(obj1 as LevelBlock).notANote)
					_player.boostByDefaultValue();
			}
		}
		
private function generateNewParts ():void
		{
			//trace(LevelPart.partWidth,LevelPart.partHeight,player.x, _prevColumn);
			if (LevelPart.partWidth==-1||LevelPart.partHeight==-1)
				return;
				
			if (_player.x>=0&&_player.x<1.5*LevelPart.partWidth&&_prevColumn==3)
			{
				_prevColumn=0;
				loadColumn(2);
			}
			if (_player.x>=1.5*LevelPart.partWidth&&_player.x<2.5*LevelPart.partWidth&&_prevColumn==0)
			{
				_prevColumn=1;
				loadColumn(3);
				copyColumn(3, 0);
			}
			if (_player.x>=2.5*LevelPart.partWidth&&_player.x<3.5*LevelPart.partWidth&&_prevColumn==1)
			{
				_prevColumn=2;
				loadColumn(1);
			}
			if (_player.x>=3.5*LevelPart.partWidth&&_prevColumn==2)
			{
				_prevColumn=3;
				_player.x-=3*LevelPart.partWidth;
			}
		}
		
		private function loadColumn(column:int):void//might cause leaking
		{
			for (var i:int=0;i<3;i++)
			{
				_collisionGroup.remove(_allParts[i][column],true);//should be leaking, i believe
				_allParts[i][column]=new LevelPart(i,column);
				_collisionGroup.add(_allParts[i][column]);
			}
			//trace (column,_prevColumn);
		}
		
		private function copyColumn (from:int, to:int):void//moar leaking
		{
			for (var i:int=0; i<3; i++)
			{
				_collisionGroup.remove(_allParts[i][to]);
				_allParts[i][to]=_allParts[i][from].clone();
				_allParts[i][to].moveTo(i,to);
				_collisionGroup.add(_allParts[i][to]);
			}
		}
		
	}
}

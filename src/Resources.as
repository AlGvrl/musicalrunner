package {
	/**
	 * @author 1
	 */
	//this class will hold all the assets, so you don't need to embed every of them in different places
	public class Resources {
		[Embed(source="../assets/level.xml")]
        public static const levelXML: Class;
		[Embed(source="../assets/sounds/C.mp3")]
		public static const nC:Class;
		[Embed(source="../assets/sounds/D.mp3")]
		public static const nD:Class;
		[Embed(source="../assets/sounds/E.mp3")]
		public static const nE:Class;
		[Embed(source="../assets/sounds/F.mp3")]
		public static const nF:Class;
		[Embed(source="../assets/sounds/G.mp3")]
		public static const nG:Class;
		[Embed(source="../assets/sounds/A.mp3")]
		public static const nA:Class;
		[Embed(source="../assets/sounds/B.mp3")]
		public static const nB:Class;
		
		[Embed(source="../assets/sounds/kick.mp3")]
		public static const bKick:Class;
		[Embed(source="../assets/sounds/snare.mp3")]
		public static const bSnare:Class;
		[Embed(source="../assets/sounds/hihat.mp3")]
		public static const bHiHat:Class;
		[Embed(source="../assets/sounds/kickandsnare.mp3")]
		public static const bKickAndSnare:Class;
		[Embed(source="../assets/sounds/discobeat.mp3")]
		public static const bBeat:Class;
	}
}

package {

	/**
	 * @author 1
	 */
	public class Notes {
		static public const C:int=0;
		static public const D:int=1;
		static public const E:int=2;
		static public const F:int=3;
		static public const G:int=4;
		static public const A:int=5;
		static public const B:int=6;
		static public const NONE:int=7;
		
		
		static public const colors:Vector.<uint>=new <uint> [0xffee1111,0xFFff8800,0xffeeee00,
			0xff22cc22,0xff88ffff, 0xff2244ff,0xffdd22ff, 0xffaaaaaa];//vectors are faster
			
		static public var sounds:Vector.<Class>;
			
		static public function init():void
		{
			sounds=new <Class> [];
			sounds.push (Resources.nC);
			sounds.push (Resources.nD);
			sounds.push (Resources.nE);
			sounds.push (Resources.nF);
			sounds.push (Resources.nG);
			sounds.push (Resources.nA);
			sounds.push (Resources.nB);
		}
	}
}

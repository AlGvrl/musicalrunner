package {
	import org.flixel.*;

	/**
	 * @author 1
	 */
	public class BeatManager extends FlxBasic {

		private var _totalMeasureTime:uint;//milliseconds
		private var _currentMeasureTime:uint;
		private var _measureStarted:uint;
		
		private var _instruments:Vector.<Instrument>;
		
	//	private var 
		public function get measureTime():uint
		{
			return _currentMeasureTime;	
		}
		public function BeatManager(measureTime:uint)
		{
			_totalMeasureTime=measureTime;
			_measureStarted=FlxU.getTicks();
			_instruments=new Vector.<Instrument>();
		}
		
		public function addInstrument(startWaitingTime:uint,timeInterval:uint,tolerance:uint,soundToLoad:Class):Instrument
		{
			_instruments.push(new Instrument(startWaitingTime, timeInterval, tolerance, soundToLoad, this));
			return _instruments[_instruments.length-1];
		}
		
		public function SelectBySound(sound:Class):Instrument
		{
			for each (var instr:Instrument in _instruments)
			{
				if (instr.unloadedSound==sound)
					return instr;
			}
			return null;
		}
		
		public override function update():void
		{
			super.update();
			
			_currentMeasureTime=FlxU.getTicks()%_totalMeasureTime;
			if (_currentMeasureTime>=_totalMeasureTime)
			{
				_currentMeasureTime=0;
				_measureStarted=FlxU.getTicks();
				//set all instruments' hasstarted to false||not needed
			}
		}
		
	}
}
